/*
 * just a namespace for utilities
 */
var util = util || {};

/*
 * Utility function that copies all properties from one object to another.
 */
util.copy = function(from, to) {
	var to = to || {};
	for ( var property in from) {
		if (from.hasOwnProperty(property)) {
			to[property] = from[property];
		}
	}
}

/*
 * Suggested to be the top parent class for all your subclasses. Responsible to
 * copy all args to this (the newly created object) and call the init function.
 * 
 * Example
 * 
 * Lets define a Panel 'Class'. That has a method init.
 * 
 * var Panel = Obj.extend(Obj, {
 * 
 * name:'Default Panel Name',
 * 
 * init : function() { console.log("Initizing Panel: "+this.name); },
 * 
 * sayName:function(){ console.log(this.name); }
 * 
 * });
 * 
 * When we create a new instance of Panel 'Class'
 * 
 * var myPanel = new Panel();
 * 
 * then the init method will be executed to initialize our panel object.
 * 
 * When we create a new instance passing arguments:
 * 
 * var myPanel = new Panel({name:"Michalis Panel",size: "600px"});
 * 
 * then the arguments will be copied into the myPanel Object(this) before calling init
 * ovverriding the properties from the prototype
 * 
 */
var Obj = Obj || function(args) {
	util.copy(args, this);
	if (this.init) {
		this.init.call(this);
	}
};

/*
 * 
 * Returns a new Child 'Class'. The Child 'Class' will inherits Parent 'Class'
 * properties, will have protoProps in its prototype  and also will have staticProps 
 * available to be accessed in a static way : 
 * Example of static access: 
 *	Child.oneStaticProperty or call to a static function Child.oneStaticFunction()
 * 
 * @param {'class'} parent:     The parent 'class' 
 * @param {object} protoProps:  The properties to be copied to the subclass Prototype 
 * @param {object} staticProps: The properties to be copied to the subclass it self.
 *                               Accessed later in a static way
 * 
 * 
 */
Obj.extend = function(Parent, protoProps, staticProps) {

	// if no parent 'Class' specified create one
	var Parent = Parent || function() {
	};

	// This is the 'class' to return:
	var Child;

	// If we pass constructor in the protoProps use this as child.
	if (protoProps && protoProps.hasOwnProperty('constructor')) {
		Child = protoProps.constructor;
	} else {
		// Create the new Child class (constructor)
		Child = function() {
			// call the Parent contructor when instantiating this class
			return Parent.apply(this, arguments);
		};
	}

	// Copy to Clild all Parent static properties and all staticProps
	util.copy(Parent, Child);
	util.copy(staticProps, Child);

	/*
	 * We now need to inherit the parent if we do it like this 
	 * Child.prototype =new Parent(); 
	 * then the Parent contructor will be executed which is not desirable
	 * so we use the Surrogate pattern to inherit from the Parent
	 * 
	 */
	var Surrogate = function() {
		this.constructor = Child;
	};

	Surrogate.prototype = Parent.prototype;
	Child.prototype = new Surrogate;

	// Copy all the protoProps to the child prototype
	if (protoProps) {
		util.copy(protoProps, Child.prototype);
	}

	// Hold a reference to Parent Class
	Child.__super__ = Parent.prototype;

	return Child;
};

/*
 * EXAMPLES
 */

var Panel = Obj.extend(Obj, {

	name : 'Default Panel Name',

	init : function() {
		console.log("Initizing Panel: " + this.name);
	},

	sayName : function() {
		console.log(this.name);
	},

});

var SubPanel = Obj.extend(Panel, {

	name : 'SubPanel name',
	
	init : function() {
		//may want to initialize parent first
		SubPanel.__super__.init();
		console.log("Initizing SubPanel: " + this.name);
	}

}, {
	oneStaticFunction : function() {
		console.log("I am a static function");
	}
});

var myPanel = new SubPanel();
myPanel.sayName();

//static call
SubPanel.oneStaticFunction();

//put properties to this
var myPanel2 = new SubPanel({name:'Name from this'});
myPanel2.sayName();

